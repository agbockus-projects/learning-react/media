import UsersList from './components/UsersList'

function App() {

  return (
    <div className="app container mx-auto">
      <h1>Media List Application</h1>
      <div>
        <UsersList />
      </div>
    </div>
  );
}

export default App;
