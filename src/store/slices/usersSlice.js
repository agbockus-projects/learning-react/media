import { createSlice } from '@reduxjs/toolkit'
import { fetchUsers } from '../thunks/fetchUsers'
import { addUser } from '../thunks/addUser'
import { removeUser } from '../thunks/removeUser'

const usersSlice = createSlice({
  name: 'users',
  initialState: {
    isLoading: false,
    error: null,
    data: [],
  },
  reducers: {},
  extraReducers(builder) {

    builder.addCase(fetchUsers.pending, (state, action) => {
      state.isLoading = true
    })

    builder.addCase(fetchUsers.fulfilled, (state, action) => {
      state.data = action.payload
      state.isLoading = false
    })

    builder.addCase(fetchUsers.rejected, (state, action) => {
      state.error = action.error
      state.isLoading = false
    })

    builder.addCase(addUser.pending, (state, action) => {
      state.isLoading = true
    })

    builder.addCase(addUser.fulfilled, (state, action) => {
      state.data.push(action.payload)
      state.isLoading = false
    })

    builder.addCase(addUser.rejected, (state, action) => {
      state.error = action.error
      state.isLoading = false
    })

    builder.addCase(removeUser.pending, (state, action) => {
      state.isLoading = true
    })

    builder.addCase(removeUser.fulfilled, (state, action) => {
      state.data = state.data.filter((user) => { return user.id !== action.payload.id })
      state.isLoading = false
    })

    builder.addCase(removeUser.rejected, (state, action) => {
      state.error = action.error
      state.isLoading = false
    })
  },
})

export const usersReducer = usersSlice.reducer
